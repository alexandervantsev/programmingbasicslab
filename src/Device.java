
public class Device 
{
	private String name;
	private Integer condition;
	private Crystal crystal;
	private Double zeroGravityValue;
	private State state;
	private Integer zeroGravityDirection;
	
	public Device(String Name)
	{
		this.name = Name;
		this.condition=100;
		this.crystal=null;
		this.zeroGravityDirection = 0;
		this.zeroGravityValue = 0.0;
		this.state = State.Off;
	}
	public void setCrystal(Crystal crystal)
	{
		this.crystal = crystal;
	}
	
	public void setCondition(int value)
	{
		if (this.condition <= -100)
		{
			throw new MyError("Device has been completely destroyed.");
		}
		if (this.condition <= 100)
		{
			this.condition += value;
		}
	}
	
	public int getCondition()
	{
		return this.condition;
	}
	
	public void sw()
	{
		if (state == State.On)
		{
			this.zeroGravityValue = 0.0;
			state = State.Off;
		}
		else
		{
			if (crystal != null)
			{
				this.zeroGravityValue = this.crystal.getSize().doubleValue() * 100;
				state = State.On;
			}		
		}
	}
	
	public void setGravityDirection(int value)
	{
		this.zeroGravityDirection+=value;
	}
	
	
	
	public String toString()
	{
		return 	this.name + "\n" + "Crystal: " + (this.crystal == null ? "is not set!" : this.crystal) + "\n" + "Condition: " + this.condition +"\n";
	}
	
	
	public boolean equals(Object o)
	{
		 if (o == null)
		       return false;
		 if (o == this)
		       return true;
		 if (o.getClass() != getClass())
		       return false;	 
		 if (	(this.name == ((Device)o).name) && 
				(this.crystal == ((Device)o).crystal) && 
				(this.condition == ((Device)o).condition)
		 )
			 return true;
		 else
			 return false;
	}
	

	 public int hashCode()
	 {
		return this.name == null ? 0 : this.name.hashCode();
	 }
}

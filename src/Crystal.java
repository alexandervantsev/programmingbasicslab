import java.io.Serializable;
import java.util.*;

public class Crystal implements Comparable<Crystal>, Serializable
{	
	
	protected Double size;
	protected Integer shine;
	protected Color color;
	public Crystal( Double Size, int Shine, Color color )
	{
		this.size = Size;
		this.shine=Shine;
		this.color=color;
	}
	
	public void changeGravityDirection(Device device)
	{
		device.setGravityDirection(10);
	}
	public Double getSize()
	{
		return this.size;
	}

	@Override
	public int compareTo(Crystal arg0) {
		int colorDiff = this.color.compareToIgnoreCase(arg0.color);
		if (colorDiff !=0)
		{
			return colorDiff;
		}
		int sizeDiff = this.size.compareTo(arg0.size);
		if (sizeDiff !=0)
		{
			return sizeDiff;
		}
		return this.shine - arg0.shine;
	}

}

import java.io.*;
import java.util.*;

public class CrystalCollection {


	private TreeSet<Crystal> crystalCollection;
	
	public CrystalCollection()
	{
		this.crystalCollection = new TreeSet<Crystal>();
	}

	public CrystalCollection(Collection<? extends Crystal> anotherCollection)
	{
	}
	
	public TreeSet<Crystal> getCrystalCollection() {
		return crystalCollection;
	}

	public void setCrystalCollection(TreeSet<Crystal> crystalCollection) {
		this.crystalCollection = crystalCollection;
	}
	
	public void SerializeToXML(){
		FileWriter fw;
		try{
			fw = new FileWriter(new File(System.getenv().get("jlp")));
			fw.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			fw.write(System.lineSeparator());
			fw.write("<crystalcollection>");
			for(Crystal cr : this.getCrystalCollection()){
				fw.write(System.lineSeparator());
				fw.write("<crystal>");
				fw.write(System.lineSeparator());
				fw.write("<size>" + cr.size.toString() + "</size>");				
				fw.write(System.lineSeparator());
				fw.write("<shine>" + cr.shine.toString() + "</shine>");
				fw.write(System.lineSeparator());
				fw.write("<color>" + cr.color.toString() + "</color>");
				fw.write(System.lineSeparator());
				fw.write("</crystal>");
			}
			fw.write(System.lineSeparator());
			fw.write("</crystalcollection>");
			fw.close();
		}
		catch(IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public void DeserializeFromXML(String path){
		TreeSet<Crystal> crystals = new TreeSet<Crystal>();
		Scanner sc;
		try {
			sc = new Scanner(new File(path));
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			return;
			
		}
		String currentLine = new String();
		currentLine = sc.nextLine();
		if (currentLine.isEmpty()){
			System.err.println("File is empty");
			return;
		}
		String ss = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		if (!currentLine.equals(ss)) {
			System.err.println("Wrong version or encoding of the file.");
			return;
		}
		currentLine = sc.nextLine();
		if (!currentLine.equals("<crystalcollection>")) {
			System.err.println("There is no XML tag <crystalcollection>.");
			return;
		
		}
		currentLine = sc.nextLine();
		if (currentLine.equals("</crystalcollection>")){
			this.setCrystalCollection(crystals);
			return;
		}
		else if (!currentLine.equals("<crystal>"))		{
			System.err.println("There is no closing tag </crystalcollection>.");
			return;
		}
		else {
			while (!currentLine.equals("</crystalcollection>")){
				Crystal cr = new Crystal(0.0,0,Color.White);
				currentLine = sc.nextLine();
				
				if (currentLine.startsWith("<size>") & currentLine.endsWith("</size>")) try {
					Double size = Double.parseDouble(currentLine.substring(6, currentLine.length() - 7));
					cr.size = size;
				}
				catch(Exception ex){
					System.err.println("Size should be a Double value.");
					return;
				}
				else {
					System.err.println("There is a mistake in tags <size></size>.");
					return;
				}
				
				currentLine = sc.nextLine();
				
				if (currentLine.startsWith("<shine>") & currentLine.endsWith("</shine>")) try {
					Integer shine = Integer.parseInt(currentLine.substring(7, currentLine.length() - 8));
					cr.shine = shine;
				}
				catch(Exception ex){
					System.err.println("Shine should be an Integer value.");
					return;
				}
				else {
					System.err.println("There is a mistake in tags <shine></shine>.");
					return;
				}
				
				currentLine = sc.nextLine();
				
				if (currentLine.startsWith("<color>") & currentLine.endsWith("</color>")) {
					String color = currentLine.substring(7, currentLine.length() - 8);
					if (color.equals("White")) {cr.color = Color.White;} 
					else if (color.equals("Blue")) {cr.color = Color.Blue;}
					else {
						System.err.println("Color should be either white or blue."); 
						return;
					}
				}
				else{
					System.err.println("There is a mistake in tags <color></color>");
					return;
				}
					
				
				crystals.add(cr);
				currentLine = sc.nextLine();
				if (!currentLine.equals("</crystal>")){
					System.err.println("There should have been closing tag </crystal>.");
				}
				currentLine = sc.nextLine();
				if (!currentLine.equals("<crystal>") & !currentLine.equals("</crystalcollection>")){
					System.err.println("There should have been closing tag </crystalcollection>.");
				}
			}
			try {
				currentLine = sc.nextLine();
					System.err.println("There are some useless characters after </crystalcollection>.");
					return;
			}
			catch(NoSuchElementException e){}
		
		}
		sc.close();
		this.setCrystalCollection(crystals);
	}
}

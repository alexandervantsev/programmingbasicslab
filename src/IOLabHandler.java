import java.util.Scanner;
import org.json.*;
import java.util.TreeSet;

public class IOLabHandler {
	public static volatile boolean keepRunning = true;
	public IOLabHandler(){};
	
	public void handleRequests(CrystalCollection cc){
		
		System.out.println("This program works with the collection of Crystals. Write \"help\" to read about commands.");
		Scanner sc = new Scanner(System.in);
		String currentToken = new String();
		String currentCommand = new String();
		String currentArgument = new String();
		while (keepRunning){
			try{
				currentToken = sc.nextLine();
				System.out.println(currentToken);
			}
			catch(Exception e) {
				IOLabHandler.keepRunning = false;
				System.err.println(e.getMessage());
			}
			if (currentToken.contains("{")){				
				try{
					currentCommand = currentToken.substring(0, currentToken.indexOf("{")).trim();
					currentArgument = currentToken.substring(currentToken.indexOf("{"));
					while (!currentArgument.contains("}")){
						currentToken = sc.nextLine();
						currentArgument += currentToken;
					}
				}
				catch(StringIndexOutOfBoundsException ex){
					continue;
				}
			}
			else {
				currentCommand = currentToken.trim();
			}
			switch (currentCommand){
				case "help" : System.out.println("add_if_min {...} - to add new crystal, if it's less than others. \n There should be three args: \n color (White < Blue), size (Real), shine (Real). \n"
					+ "clear - to clear all the collection.\n"
					+ "info - to learn some information about the collection.\n"
					+ "remove {...} - to remove the crystal from the collection.\n"
					+ "import {...} - to import collection from a file. Path should be given as an argument.\n"
					+ "exit - obviously, to exit."); break;
				case "add_if_min": addIfMin(cc, currentArgument); break;
								
				case "clear": clear(cc); break;
					
				case "info": info(cc); break;
				
				case "remove": remove(cc, currentArgument); break;
				
				case "import": importSomeCrystals(cc, currentArgument); break;
				case "exit": keepRunning = false; break; 
				default: System.err.println("There is no such a command.");
				
			}
			
		}
	}
	//{"Color":"White","size":2.0,"shine":5}
	/** Removes a specific element from the collection
	 * @param cc - collection which the element will be removed from
	 * @param curArg - string that defines an element by its value
	 */
	public void remove(CrystalCollection cc, String curArg){
		if (!checkIfArgumentExists(curArg)){
			return;
		}
		Crystal cr = makeNewCrystal(curArg);
		if (cr == null) {
			return;
		}
		boolean ifAnyCrystalWasRemoved = false;
		for(Crystal anotherCrystal:cc.getCrystalCollection()){
			if(cr.compareTo(anotherCrystal) == 0){
				ifAnyCrystalWasRemoved = true;
				cc.getCrystalCollection().remove(anotherCrystal);
				System.out.println("Crystal has been removed.");
				break;
			}
		}
		if (ifAnyCrystalWasRemoved == false){
			System.out.println("There was not found such a crystal.");
		}
	}
	
	/**Replaces a collection with a empty one
	 * @param cc - collection
	 * *
	 * @param cc
	 */
	public void clear(CrystalCollection cc){
		TreeSet<Crystal> cr = new TreeSet<Crystal>();
		cc.setCrystalCollection(cr);
		System.out.println("Collection is empty now.");
	}
	
/**
 * Adds new element to the collection if it's less than a smallest one
 * @param cc - collection
 * @param curArg - string that defines an element by its value
 */
	public void addIfMin(CrystalCollection cc, String curArg){
		if (!checkIfArgumentExists(curArg)){
			return;
		}
		try {
			Crystal cr = makeNewCrystal(curArg);
			if (cr == null){
				return;
			}
			if (!cc.getCrystalCollection().isEmpty()){
				if (cr.compareTo(cc.getCrystalCollection().first())<0){
					cc.getCrystalCollection().add(cr);
					System.out.println("Crystal has been added succesfully.");
				}
				else {
					System.err.println("There is a smaller or similar crystal in the collection.");
				}
			}
			else{
				cc.getCrystalCollection().add(cr);
			}
		}
		catch(NullPointerException ex){
		}
	}
	
	/**
	 * adds all the crystals from the file to the collection
	 * @param cc - collection
	 * @param curArg - path to the file
	 */
	public void importSomeCrystals(CrystalCollection cc, String curArg){
		if (!checkIfArgumentExists(curArg)){
			return;
		}
		String path = curArg.substring(curArg.indexOf("{")+1, curArg.indexOf("}"));
		CrystalCollection additionalCrystals = new CrystalCollection();
		additionalCrystals.DeserializeFromXML(path);
		for(Crystal cr : additionalCrystals.getCrystalCollection())	{
			cc.getCrystalCollection().add(cr);
		}
	}
/**gives some info about the collection
 * 
 * @param cc - collection
 */
	public void info(CrystalCollection cc){
		System.out.println("There are " + cc.getCrystalCollection().size() + " elements in the collection. They are of Crystal type.");
	}
	
	public Crystal makeNewCrystal(String arg){
		try{
			JSONObject obj = new JSONObject(arg);
			
			Double size = obj.getDouble("size");
			Integer shine = obj.getInt("shine");
			String color = obj.getString("color").toLowerCase();
			Color col = Color.White;
			switch (color){
				case "white" : col = Color.White; break;
				case "blue": col = Color.Blue; break;
				default : System.out.println("Wrong color."); return null;
			}
			Crystal cr = new Crystal(size,shine,col);
			return cr;
		}catch(Exception ex){
			System.err.println("Argument is probably in a wrong format.");
		}
		return null;
		
		

				
	}
	
	private boolean checkIfArgumentExists(String arg){
		if (arg.isEmpty()){
			System.err.println("There is no argument.");
			return false;
		}
		return true;
	}
}


import java.util.NoSuchElementException;


public class Main {
	static CrystalCollection crystals = new CrystalCollection();
	
	public static void main(String[] args) 
	{
		
		try{
			String path = System.getenv().get("jlp");
			crystals.DeserializeFromXML(path);
		}
		catch(NoSuchElementException nsee){
			System.err.println("File is empty.");
		}
		catch(Exception ex){
			System.err.println("Environmental variable must show the path to the file with XML.");
		}
		finally{}
		
		IOLabHandler iolh = new IOLabHandler();
		
		final Thread mainThread = Thread.currentThread();
		Runtime.getRuntime().addShutdownHook(new Thread() {
	    public void run() {
	    	try{
	    		IOLabHandler.keepRunning = false;
	    		mainThread.join();
	    		try{		
	    			crystals.SerializeToXML();
	    		}
	    		catch(NullPointerException npe){
	    			System.err.println("There were nothing to save.");
	    		}
	    	}
	    	catch(Exception e){
	    		
	    	}
	    }
	});
		iolh.handleRequests(crystals);
		
		try{		
			crystals.SerializeToXML();
		}
		catch(NullPointerException npe){
			System.err.println("There were nothing to save.");
		}
	}
}

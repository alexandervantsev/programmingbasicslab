
public class MyError extends Error 
{
	private String errorMsg;
	
	MyError(String errMsg)
	{
		this.errorMsg = errMsg;
	}
	
	public String getMessage()
	{
		return errorMsg;
	}
}
